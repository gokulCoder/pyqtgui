# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pushButton_gui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(421, 267)
#        MainWindow.setFixedSize(421, 267)
        
        font = QtGui.QFont()
        font.setPointSize(22)
        MainWindow.setFont(font)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.clickButton = QtWidgets.QPushButton(self.centralwidget)
        self.clickButton.setGeometry(QtCore.QRect(110, 80, 171, 81))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.clickButton.setFont(font)
        self.clickButton.setCheckable(False)
        self.clickButton.setObjectName("clickButton")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(80, 40, 231, 41))
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label.setFont(font)
        self.label.setObjectName("label")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        self.userApp()
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.count = 0

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Hello world"))
        MainWindow.statusBar().setSizeGripEnabled(False) 
        MainWindow.setFixedSize(MainWindow.size())
        MainWindow.setWindowIcon(QtGui.QIcon("C:\\Users\\user\\Documents\\PyQt_GUI\\Button\\software.png"))
        self.clickButton.setText(_translate("MainWindow", "Click Me"))
        self.label.setText(_translate("MainWindow", "Click Bellow button"))

    def userApp(self):
        self.clickButton.clicked.connect( self.printText )
    
    def printText(self):
        if(self.count % 2):
            self.label.setGeometry(QtCore.QRect(80, 40, 231, 41))
        else:
            self.label.setGeometry(QtCore.QRect(80, 160, 231, 41))
        self.label.setText("    Button clicked !")
        self.count = self.count + 1

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

