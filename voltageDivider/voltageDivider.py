import sys
import time
from PyQt5 import QtGui, QtWidgets, QtCore, uic

clicks = 0
app = QtWidgets.QApplication([])
dig = uic.loadUi("voltageDivider.ui")


def outputVoltage():
  """ vout = vin*(R2/(R1+R2)) """
  QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
  
  if (dig.comboBox_r1.currentIndex() == 1):
    r1_multiply = 1000
  elif(dig.comboBox_r1.currentIndex() == 2):
    r1_multiply = 1000000
  else:
    r1_multiply = 1
  
  if (dig.comboBox_r2.currentIndex() == 1):
    r2_multiply = 1000
  elif(dig.comboBox_r2.currentIndex() == 2):
    r2_multiply = 1000000
  else:
    r2_multiply = 1

  try:
    vin = int(dig.lineEdit_vin.text()) 
    r1  = int(dig.lineEdit_r1 .text()) * r1_multiply
    r2  = int(dig.lineEdit_r2 .text()) * r2_multiply
    outputVolt = vin * (r2/(r1+r2)) 
  except:
    dig.statusbar.showMessage ("Invalid inputs", 1000)
    outputVolt = ''
  
  time.sleep(0.5)
  QtWidgets.QApplication.restoreOverrideCursor()

  dig.lineEdit_vout.setText(str(outputVolt))

#Main program
dig.lineEdit_vin.setFocus()

onlyInt = QtGui.QIntValidator()
dig.lineEdit_vin.setValidator(onlyInt)
dig.lineEdit_r1 .setValidator(onlyInt)
dig.lineEdit_r2 .setValidator(onlyInt)

dig.lineEdit_vout.setReadOnly(True)
items = ['ohm', 'K ohm', 'M ohm']
dig.comboBox_r1.addItems(items)
dig.comboBox_r2.addItems(items)
dig.pushButton_calc.clicked.connect(outputVoltage)

#Menu
def manuSelect(selection):
  if(selection.text() == 'Clear'):
    dig.lineEdit_vin .clear()
    dig.lineEdit_r1  .clear()
    dig.lineEdit_r2  .clear()
    dig.lineEdit_vout.clear()
  
  if(selection.text() == 'About'):
    QtWidgets.QMessageBox.about( dig, "About", " Voltage Divider Version 1.0 \nAuthor : gok@lucastvs.co.in ")

dig.menuBar.triggered[QtWidgets.QAction].connect(manuSelect)
dig.setWindowIcon(QtGui.QIcon("img\\res.png"))

dig.show()
app.exec()
