import plotly
import plotly.graph_objs as go
import pandas as pd
import time
import subprocess

df = pd.read_csv("testfile.txt")

wave1 = go.Scatter(
        x=df['series'],
        y=df['data'],
        name='data 1')

wave2 = go.Scatter(
        x=df['series'],
        y=df['date2'],
        name='date 2')

data = [wave1,wave2]

config = {
          'linkText'      : " ",
          'displayModeBar': True,
          'scrollZoom'    : True
          }

url = plotly.offline.plot(data,filename='Changes.html',auto_open=False, config=config)
subprocess.call([r'C:\Program Files\Mozilla Firefox\firefox.exe', url])
