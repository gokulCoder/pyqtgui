import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 2, 100)
rand_y = np.random.random(len(x))

plt.plot(x, x, label='linear')
plt.plot(x, x**2, label='quadratic')
plt.plot(x, x**3, label='cubic')
plt.plot(x, rand_y+x**3, label = 'random')


plt.xlabel('x label')
plt.ylabel('y label')

plt.title("Simple Plot")

plt.legend()

plt.show()
